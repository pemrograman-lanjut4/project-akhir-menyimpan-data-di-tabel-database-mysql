import javax.swing.*; // Import library
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*; // Menggunakan MySQL

// Aldo Kristiawan 235150401111009 Pemrograman Lanjut Project Akhir
public class AplikasiSiswa extends JFrame {
    // Deklarasi atribut
    private JTable tabel;
    private DefaultTableModel model;
    private JTextField namaField;
    private JTextField umurField;
    private Connection koneksi;

    public AplikasiSiswa() {
        // Inisialisasi JFrame (frame utama aplikasi)
        setTitle("Database Siswa");
        setSize(600, 400);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);

        // Persiapan isi GUI
        // Tabel dengan kolom ID, nama, dan umur
        model = new DefaultTableModel(new String[]{"ID", "Nama", "Umur"}, 0);
        // Tabel untuk menampilkan data siswa
        tabel = new JTable(model);
        // Scroll pane untuk scroll tabel ke atas bawah
        JScrollPane scrollPane = new JScrollPane(tabel);

        // Field untuk memasukkan data nama dan umur
        namaField = new JTextField(15);
        umurField = new JTextField(3);
        // Tombol untuk add siswa baru
        JButton tambahButton = new JButton("Tambah"); 

        // Panel input untuk penataan field dan tombol
        JPanel inputPanel = new JPanel();
        inputPanel.add(new JLabel("Nama:"));
        inputPanel.add(namaField);
        inputPanel.add(new JLabel("Umur:"));
        inputPanel.add(umurField);
        inputPanel.add(tambahButton);

        // Tambahkan scroll pane di tengah
        add(scrollPane, BorderLayout.CENTER);
        // Tambahkan panel input di bagian bawah
        add(inputPanel, BorderLayout.SOUTH); 

        // Action listener untuk tombol "Tambah"
        tambahButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // Panggil method tambahSiswa() ketika tombol "Tambah" ditekan
                tambahSiswa();
            }
        });

        // Connect ke database
        // Method untuk menghubungkan ke database
        koneksiKeDatabase();
        // Method untuk cetak/menampilkan data siswa dari database
        muatSiswa();

        // Menampilkan frame
        setVisible(true);
    }

    private void koneksiKeDatabase() {
        try {
            // Connect ke MySQL database
            koneksi = DriverManager.getConnection("jdbc:mysql://localhost:3306/sekolah", "root", "Aldo0909");
            System.out.println("Koneksi berhasil!");
        } catch (SQLException e) {
            // Print stack trace jika terjadi error
            e.printStackTrace();
        }
    }

    private void muatSiswa() {
        try {
            Statement stmt = koneksi.createStatement(); // Membuat statement untuk menjalankan query
            ResultSet rs = stmt.executeQuery("SELECT * FROM siswa"); // Jalankan query untuk mengambil semua data siswa

            // Iterasi melalui hasil query
            while (rs.next()) {
                int id = rs.getInt("id"); // Ambil ID siswa yang diinput user

                String nama = rs.getString("nama"); // Ambil nama siswa yang diinput user

                int umur = rs.getInt("umur"); // Ambil umur siswa yang diinput user

                model.addRow(new Object[]{id, nama, umur}); // Tambahkan baris baru ke tabel model
            }
        } catch (SQLException e) {
            e.printStackTrace(); // Print stack trace jika terjadi error
        }
    }

    private void tambahSiswa() {
        String nama = namaField.getText(); // Ambil teks dari field nama
        int umur = Integer.parseInt(umurField.getText()); // Ambil teks dari field umur dan convert ke integer

        try {
            // Siapkan statement untuk memasukkan data baru
            PreparedStatement pstmt = koneksi.prepareStatement("INSERT INTO siswa (nama, umur) VALUES (?, ?)");
            pstmt.setString(1, nama); // Set nilai untuk parameter pertama (nama)
            pstmt.setInt(2, umur); // Set nilai untuk parameter kedua (umur)
            pstmt.executeUpdate(); // Jalankan statement untuk memasukkan data

            // Refresh tabel
            model.setRowCount(0); // Hapus semua baris dari tabel model
            muatSiswa(); // Muat ulang data siswa dari database
        } catch (SQLException e) {
            e.printStackTrace(); // Print stack trace jika terjadi error
        }
    }

    public static void main(String[] args) {
        // Jalankan aplikasi GUI pada thread event dispatch
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new AplikasiSiswa(); // Buat instance dari AplikasiSiswa
            }
        });
    }
}
